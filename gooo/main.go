package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type Article struct {
	Title       string `json:"Title"`
	Description string `json:"Desc"`
	Content     string `json:"Content"`
}

var Articles []Article

func handleRequest() {
	r := mux.NewRouter()
	http.HandleFunc("/", homepage)
	http.HandleFunc("/all", all)
	log.Fatal(http.ListenAndServe(":7000", nil))
}

func all(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Printing All Articles.Endpoint hit.")
	fmt.Fprintf(w, "All Articles \n ")
	json.NewEncoder(w).Encode(Articles)
}

func homepage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi %s Home Page\n\n", r.URL.Path)
}

func main() {
	Articles = []Article{
		{Title: "Title-1", Description: "Description-1", Content: "Content-1"},
		{"Title-2", "Description-2", "Content-2"},
	}
	handleRequest()
}
