openssl genrsa -out john.key | 2048
openssl req -new -key john.key -out john.csr
cat john.csr | base64 | tr -d "\n"

1. openssl genrsa -out u1.key
2. openssl req -new -key u1.key -out u1.csr -subj "/CN=u1/O=g1"
3. cat u1.csr | base64 | tr -d '\n'
4. echo "" | base64 --decode
5. k get csr -o jsonpath={.items[0].status.certificate} | base64 --decode


openssl req -new -newkey rsa:4096 -nodes -keyout bob-k8s.key -out bob-k8s.csr -subj "/CN=bob/O=devops"
kubectl config set-cluster $(kubectl config view -o jsonpath='{.clusters[0].name}') --server=$(kubectl config view -o jsonpath='{.clusters[0].cluster.server}') --certificate-authority=k8s-ca.crt --kubeconfig=bob-k8s-config --embed-certs
k config set-credentials $(kubectl config view -o jsonpath='{.clusters[1].name}')  --server=$(kubectl config view -o jsonpath='{.clusters[1].cluster.server}') --certificate-authority=k8s-ca.crt --kubeconfig=bob-k8s-config --embed-certs
