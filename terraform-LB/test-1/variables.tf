variable "network_name" {
  default = "tf-gke-k8s"
}
variable "region" {
  default = "asia-south1"
}
variable "project" {
  default = "k8s-1-286619"
}
variable "zone" {
  default = "asia-south1-a"
}